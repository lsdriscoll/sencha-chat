/**
 * Created by lee on 17/12/2015.
 */
Ext.define('chat.store.Chat', {
    extend: 'Ext.data.Store',

	alias: 'store.chat',
	storeId: 'chat',

	config: {
		socket: null
	},

	mixins  : [
		'GVB.ux.mixin.SocketIo'
	],

	requires: [
		'chat.model.Chat'
	],

	model: 'chat.model.Chat',

	proxy: {
		type: 'memory',
		reader: {
			type: 'json'
		}
	},

	socket: null,

	constructor : function() {
		this.callParent(arguments);
		console.log("connect");
		this.setSocket(this.socket);
		this.initSocket();
	}
    /*
    Uncomment to use a specific model class
    model: 'User',
    */

    /*
    Fields can also be declared without a model class:
    fields: [
        {name: 'firstName', type: 'string'},
        {name: 'lastName',  type: 'string'},
        {name: 'age',       type: 'int'},
        {name: 'eyeColor',  type: 'string'}
    ]
    */

    /*
    Uncomment to specify data inline
    data : [
        {firstName: 'Ed',    lastName: 'Spencer'},
        {firstName: 'Tommy', lastName: 'Maintz'},
        {firstName: 'Aaron', lastName: 'Conran'}
    ]
    */
});
