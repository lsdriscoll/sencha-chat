/**
 * Created by lee on 17/12/2015.
 */
Ext.define('chat.view.main.Main', {
	extend: 'Ext.container.Container',
	alias: 'widget.app-main',

	requires: [
		'chat.view.chat.Chat',
		'chat.view.main.MainController',
		'chat.view.main.MainModel'
	],

	controller: 'main',
	viewModel: 'main',

    /*
    Uncomment to give this component an xtype
    xtype: 'main',
    */

    items: [{
		xtype: 'messagelist',
		socketHost: 'http://localhost:3000'
	}]
});
