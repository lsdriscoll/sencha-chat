/**
 * Created by lee on 17/12/2015.
 */
Ext.define('chat.view.chat.Chat', {
    extend: 'Ext.grid.Panel',

	alias: 'widget.messagelist',

	requires: [
		'chat.store.Chat',
		'chat.view.chat.ChatController',
		'chat.view.chat.ChatModel'
	],

	viewModel: {
        type: 'chat'
    },

    controller: 'chat',

	//bind: {
	//	store: '{chat}'
	//},

    items: [
        /* include child components here */
    ],

	socketHost: null,

	columns: [{
		dataIndex: 'avatar',
		flex: 0.1,
		xtype: 'templatecolumn',
		tpl: '<img src="{avatar}" />'
	}, {
		dataIndex: 'message',
		flex: 0.9
	}],

	sortableColumns: false,

	hideHeaders: true,

	constructor: function(config){
		var me = this;

		Ext.apply(this, config);

		var socket = me.socket = io.connect(me.socketHost);

		Ext.apply(me, {
			store : new chat.store.Chat({
				socket : socket
			})
		});

		this.callParent(arguments);
	}
});
