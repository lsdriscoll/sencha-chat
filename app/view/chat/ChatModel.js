/**
 * Created by lee on 17/12/2015.
 */
Ext.define('chat.view.chat.ChatModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.chat',

	requires: [
		'chat.store.Chat'
	],

	stores: {
		chat: 'chat'
    },

    data: {
        /* This object holds the arbitrary data that populates the ViewModel and is then available for binding. */
    }
});
